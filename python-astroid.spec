%global _empty_manifest_terminate_build 0
Name:           python-astroid
Version:        3.3.5
Release:        1
Summary:        An abstract syntax tree for Python with inference support.
License:        LGPL-2.1-only and GPL-2.0-or-later
URL:            https://github.com/pylint-dev/astroid
Source0:        https://github.com/pylint-dev/astroid/archive/v%{version}/astroid-%{version}.tar.gz
BuildArch:      noarch
%description
An abstract syntax tree for Python with inference support.
The aim of this module is to provide a common base representation of python
source code. It is currently the library powering pylint capabilities.

%package -n python3-astroid
Summary:        An abstract syntax tree for Python with inference support.
Provides:       python-astroid = %{version}-%{release}
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:	python3-editables
BuildRequires:	python3-hatch-vcs
BuildRequires:	python3-hatchling
BuildRequires:  python3-pytest-runner
# General requires
BuildRequires:  python3-lazy-object-proxy
BuildRequires:  python3-six
BuildRequires:  python3-typed-ast >= 1.3.0
BuildRequires:  python3-wrapt
# General requires
AutoReq:        no 
Requires:       python3-lazy-object-proxy
Requires:       python3-wrapt
Requires:       python3-pytest-runner
# General requires
%description -n python3-astroid
An abstract syntax tree for Python with inference support.
The aim of this module is to provide a common base representation of python
source code. It is currently the library powering pylint capabilities.

%package help
Summary:        An abstract syntax tree for Python with inference support.
Provides:       python3-astroid-doc
%description help
An abstract syntax tree for Python with inference support.
The aim of this module is to provide a common base representation of python
source code. It is currently the library powering pylint capabilities.

%prep
%autosetup -n astroid-%{version} 

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-astroid
%license LICENSE
%{python3_sitelib}/astroid
%{python3_sitelib}/astroid*.dist-info/

%changelog
* Thu Oct 17 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 3.3.5-1
- Update package to version 3.3.5
- Fix Python 3.13 compatibility re: collections.abc
  Control setting local nodes outside of the supposed local's constructor

* Sun Sep 29 2024 wangkai <13474090681@163.com> - 3.3.4-1
- Update to 3.3.4

* Thu Aug 8 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 3.3.1-1
- update version to 3.3.1
- Fix a crash introduced in 3.3.0 involving invalid format strings.
- Add support for Python 3.13.
- Remove support for Python 3.8 (and constants PY38, PY39_PLUS, and PYPY_7_3_11_PLUS).
- Add the __annotations__ attribute to the ClassDef object model.
- Implement inference for JoinedStr and FormattedValue
- Add support for ssl.OP_LEGACY_SERVER_CONNECT (new in Python 3.12).

* Mon Jul 22 2024 lixiaoyong <lixiaoyong@kylinos.cn> - 3.2.4-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update version to 3.2.4
       Avoid reporting unary/binary op type errors when inference is ambiguous
       Fix AssertionError when inferring a property consisting of a partial function

* Tue Jun 04 2024 liuzhilin <liuzhilin@kylinos.cn> - 3.2.2-1
- Update package to version 3.2.2
- Improve inference for generic classes using the PEP 695 syntax (Python 3.12).
- Fix RecursionError in infer_call_result() for certain __call__ methods.
- Add AstroidManager.prefer_stubs attribute to control the astroid 3.2.0 feature that prefers stubs.
- Fixes a problem that six.moves brain was not effective if six.moves

* Sun Feb 25 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 3.1.0-1
- Update package to version 3.1.0
  Fix coverage artifact combination step

* Tue Nov 14 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 3.0.1-1
- Update package to version 3.0.1

* Wed Oct 11 2023 shangjiwei <cyrus_shang@163.com> - 2.15.6-1
- update to 2.17.5

* Sun Jun 25 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2.15.5-1
- Update package to version 2.15.5

* Mon May 15 2023 Dongxing Wang <dxwangk@isoftstone.com> - 2.15.4-1
- update to 2.15.4 for pylint upgrade 2.17.2

* Mon Jun 13 2022 Ge Wang <wangge20@h-partners.com> - 2.9.3-2
- fix install failure due to python-wrapt upgraded

* Sun Apr 24 2022 caodongxia <caodongxia@h-partners.com> - 2.9.3-1
- update to 2.9.3

* Mon Jul 19 2021 OpenStack_SIG <openstack@openeuler.org> - 2.5-1
- Upgrade to version 2.5

* Wed Jun 24 2020 huanghaitao <huanghaitao8@huawei.com> - 2.3.3-5
- update to fix test errors with python3.8

* Mon Feb 24 2020 Senlin Xia<xiasenlin1@huawei.com> - 2.0.4-2
- Package init
